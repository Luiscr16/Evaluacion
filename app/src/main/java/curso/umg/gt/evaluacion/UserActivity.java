package curso.umg.gt.evaluacion;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

public class UserActivity extends AppCompatActivity {
    private RadioButton rb1, rb2, rb3, rb4, rb5, rb6;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        rb1 = (RadioButton) findViewById(R.id.rb1);
        rb2 = (RadioButton) findViewById(R.id.rb2);
        rb3 = (RadioButton) findViewById(R.id.rb3);
        rb4 = (RadioButton) findViewById(R.id.rb4);
        rb5 = (RadioButton) findViewById(R.id.rb5);
        rb6 = (RadioButton) findViewById(R.id.rb6);

    }

    public void enviar(View view){
        String texto ="Su preferencia es: ";
        if (rb1.isChecked()) texto = texto + " Andorid ";
        if (rb3.isChecked()) texto =texto + " Java ";
        if (rb5.isChecked()) texto = texto +" Spring ";

        Toast.makeText(this, texto, Toast.LENGTH_SHORT).show();
    }


}
