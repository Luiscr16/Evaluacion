package curso.umg.gt.evaluacion;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    private EditText et1, et2, et3, et4;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et1 = (EditText) findViewById(R.id.et1);
        et2 = (EditText) findViewById(R.id.et2);
        et3 = (EditText) findViewById(R.id.et3);
        et4 = (EditText) findViewById(R.id.et4);
    }

    public void crear(View view){
        String pass=et3.getText().toString();
        String conpass=et4.getText().toString();

        if (pass.equals(conpass)){
            Intent i = new Intent(this, UserActivity.class);
            startActivity(i);
        }
        else
        {
            Toast notification = Toast.makeText(this, "Credenciales invalidas", Toast.LENGTH_SHORT);
            notification.show();
        }
    }

    public void cancelar(View view){
        et4.setText("");
        et3.setText("");
        et2.setText("");
        et1.setText("");
    }
}
